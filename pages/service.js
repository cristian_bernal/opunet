import React from "react";
import Layout from "@/components/layout";
import Services from "@/components/services/Services"
import MenuContextProvider from "@/context/menu-context";
import SearchContextProvider from "@/context/search-context";
import SubscribeForm from "@/components/subscribe-form";
import { fetchAPI } from "../lib/api";
import PageBanner from "@/components/page-banner";

const ServicePage = ({ globalData, servicePage, subscribers }) => {
  const {
    seo,
    headerTitle,
    headerBackground,
    services
  } = servicePage
  return (
    <MenuContextProvider>
      <SearchContextProvider>
        <Layout pageSeo={seo} globalData={globalData}>
          <PageBanner data={headerTitle} backgroundImage={headerBackground} />
          <Services services={services} />
          <SubscribeForm subscribers={subscribers} />
        </Layout>
      </SearchContextProvider>
    </MenuContextProvider>
  );
};

export async function getStaticProps() {

  const [globalData, servicePage, subscribers] = await Promise.all([
    fetchAPI("/global"),
    fetchAPI("/servicepage"),
    fetchAPI("/subscribers")
  ]);

  return {
    props: { globalData, servicePage, subscribers },
    revalidate: 1,
  }
}

export default ServicePage;
