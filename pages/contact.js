import React from "react";
import Layout from "@/components/layout";
import ContactForm from "@/components/contact/contact-form";
import PageBanner from "@/components/page-banner";
import SearchContextProvider from "context/search-context";
import MenuContextProvider from "context/menu-context";
import { fetchAPI } from "../lib/api";

const ContactPage = ({ globalData, contactPage }) => {
  const { headerTitle, formTitle, headerBackground } = contactPage

  return (
    <MenuContextProvider>
      <SearchContextProvider>
        <Layout pageSeo={{
          metaTitle: headerTitle.title,
          metaDescription: headerTitle.text,
          shareImage: headerBackground
        }} globalData={globalData}>
          <PageBanner data={headerTitle} backgroundImage={headerBackground} />
          <ContactForm data={formTitle} />
        </Layout>
      </SearchContextProvider>
    </MenuContextProvider>
  );
};

export async function getStaticProps() {

  const [globalData, contactPage] = await Promise.all([
    fetchAPI("/global"),
    fetchAPI("/contactpage"),
  ]);

  return {
    props: { globalData, contactPage },
    revalidate: 1,
  }
}

export default ContactPage;
