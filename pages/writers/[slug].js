import React from "react";
import Layout from "@/components/layout";
import PageBanner from "@/components/page-banner";
import MenuContextProvider from "context/menu-context";
import SearchContextProvider from "context/search-context";
import Articles from "@/components/blog/articles";
import { fetchAPI } from "../../lib/api";

const WriterPosts = ({ writer, globalData }) => {

    return (
        <MenuContextProvider>
            <SearchContextProvider>
                <Layout
                    pageSeo={{
                        metaTitle: writer.name,
                        metaDescription: writer.bio,
                        shareImage: writer.image
                    }}
                    globalData={globalData}>

                    <PageBanner
                        data={{
                            title: writer.name,
                            subTitle: "All posts",
                            text: writer.bio
                        }}
                    />

                    <Articles
                        data={writer.articles}
                    />
                </Layout>
            </SearchContextProvider>
        </MenuContextProvider>
    );
};

export async function getStaticPaths() {
    const writers = await fetchAPI("/writers")

    return {
        paths: writers.map((writer) => ({
            params: {
                slug: writer.slug,
            },
        })),
        fallback: false,
    }
}


export async function getStaticProps({ params }) {

    const writers = await fetchAPI(`/writers?slug=${params.slug}`)
    const globalData = await fetchAPI("/global")

    return {
        props: { writer: writers[0], globalData },
        revalidate: 1,
    }
}

export default WriterPosts;
