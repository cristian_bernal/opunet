import React from "react";
import Layout from "@/components/layout";
import SubscribeForm from "@/components/subscribe-form";
import SearchContextProvider from "@/context/search-context";
import MenuContextProvider from "@/context/menu-context";
import BetterWay from "@/components/how-we-do-it/better-way";
import OldWay from "@/components/how-we-do-it/old-way";
import PageBanner from "@/components/page-banner";
import { fetchAPI } from "../lib/api";

const HowWeDoIt = ({ globalData, howWeDoItPage, subscribers }) => {

    const {
        seo,
        headerTitle,
        headerBackground,
        oldWaySubTitle,
        oldWayTitle,
        oldWayImage,
        oldWayContent,
        oldWayFeatures,
        betterWayTitle,
        betterWayImage,
        betterWayContent,
        betterWayFeatures,
        callToActionButton
    } = howWeDoItPage

    return (
        <MenuContextProvider>
            <SearchContextProvider>
                <Layout pageSeo={seo} globalData={globalData}>
                    <PageBanner data={headerTitle} backgroundImage={headerBackground} />
                    <OldWay data={{
                        oldWayTitle,
                        oldWaySubTitle,
                        oldWayImage,
                        oldWayFeatures,
                        oldWayContent
                    }} />
                    <BetterWay data={{
                        betterWayTitle,
                        betterWayImage,
                        betterWayFeatures,
                        betterWayContent,
                        callToActionButton
                    }} />
                    <SubscribeForm subscribers={subscribers} />
                </Layout>
            </SearchContextProvider>
        </MenuContextProvider>
    );
};

export async function getStaticProps() {

    const [globalData, howWeDoItPage, subscribers] = await Promise.all([
        fetchAPI("/global"),
        fetchAPI("/how-we-do-it-page"),
        fetchAPI("/subscribers")
    ]);

    return {
        props: { globalData, howWeDoItPage, subscribers },
        revalidate: 1,
    }
}

export default HowWeDoIt;
