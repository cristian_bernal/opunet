import React from "react";
import Layout from "@/components/layout";
import PageBanner from "@/components/page-banner";
import Articles from "@/components/blog/articles";
import SearchContextProvider from "context/search-context";
import MenuContextProvider from "context/menu-context";
import { fetchAPI } from "../../lib/api";

const Category = ({ category, globalData }) => {

  return (
    <MenuContextProvider>
      <SearchContextProvider>
        <Layout pageSeo={{
          metaTitle: category.name,
          metaDescription: category.seoDescription,
          shareImage: category.backgroundImage
        }} globalData={globalData}>

          <PageBanner
            data={{
              title: category.name,
              subTitle: "Blog - Category",
              text: category.seoDescription,

            }} backgroundImage={category.backgroundImage} />

          <Articles
            data={category.articles} />

        </Layout>
      </SearchContextProvider>
    </MenuContextProvider>
  );
};

export async function getStaticPaths() {
  const categories = await fetchAPI("/categories")

  return {
    paths: categories.map((category) => ({
      params: {
        slug: category.slug,
      },
    })),
    fallback: false,
  }
}

export async function getStaticProps({ params }) {
  const category = (await fetchAPI(`/categories?slug=${params.slug}`))[0]
  const globalData = await fetchAPI("/global")

  return {
    props: { category, globalData },
    revalidate: 1,
  }
}

export default Category
