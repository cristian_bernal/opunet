import React from "react";
import Layout from "@/components/layout";
import CallToActionOne from "@/components/callToActions/call-to-action-one";
import BlogHome from "@/components/blog/blog-home";
import ParallaxOne from "@/components/parallax-1";
import SubscribeForm from "@/components/subscribe-form";
import AboutSection from "@/components/about/about-section";
import VideoHome from "@/components/video-home";
import SearchContextProvider from "@/context/search-context";
import MenuContextProvider from "@/context/menu-context";
import SliderHome from "@/components/slider-home";
import ServiceHome from "@/components/services/service-home";
import PricingPlans from "@/components/prices/prices-plans";
import WhyChooseUs from "@/components/why-choose-us";
import { fetchAPI } from "../lib/api";

const HomeOne = ({ globalData, homepageData, latestPosts, subscribers }) => {
  const {
    seo,
    slider,
    section1Title,
    section1Cards,
    section2Title,
    section2Images,
    section2Button,
    section3Title,
    section3Video,
    section3VideoPreview,
    section4Title,
    whyChooseUsTabs,
    sectionPricingTitle,
    pricingPlans,
    ecommercePlanTitle,
    ecommercePlanCard,
    callToAction,
    parallax,
    latestPostsTitle

  } = homepageData

  return (
    <MenuContextProvider>
      <SearchContextProvider>
        <Layout pageSeo={seo} globalData={globalData}>

          <SliderHome
            data={slider} />

          <ServiceHome
            data={{
              sectionTitle: section1Title,
              cards: section1Cards
            }} />

          <AboutSection
            data={{
              sectionTitle: section2Title,
              gallery: section2Images,
              button: section2Button
            }} withlineAnim />

          <VideoHome
            data={{
              sectionTitle: section3Title,
              video: section3Video,
              preview: section3VideoPreview
            }} />

          <WhyChooseUs
            data={{
              sectionTitle: section4Title,
              tabsContent: whyChooseUsTabs
            }} />

          <PricingPlans
            data={{
              sectionTitle: sectionPricingTitle,
              plans: pricingPlans,
              ecommercePlan: {
                ecommercePlanTitle,
                ecommercePlanCard,
                phoneNumber: globalData.footer.contact.phone
              }
            }} />

          <CallToActionOne
            data={callToAction} />

          <ParallaxOne
            data={parallax} />

          <BlogHome
            data={{ sectionTitle: latestPostsTitle, latestPosts }} />

          <SubscribeForm subscribers={subscribers} />
        </Layout>
      </SearchContextProvider>
    </MenuContextProvider>
  );
};

export async function getStaticProps() {

  const [globalData, homePage, latestPosts, subscribers] = await Promise.all([
    fetchAPI("/global"),
    fetchAPI("/homepage"),
    fetchAPI("/articles?_sort=date:DESC&_limit=3"),
    fetchAPI("/subscribers")
  ]);

  return {
    props: { globalData, homepageData: homePage, latestPosts, subscribers },
    revalidate: 1,
  }
}

export default HomeOne;
