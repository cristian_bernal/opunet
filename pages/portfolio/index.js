import React from "react";
import Layout from "@/components/layout";
import PageBanner from "@/components/page-banner";
import PortfolioOne from "@/components/portfolio/portfolio-one";
import MenuContextProvider from "context/menu-context";
import SearchContextProvider from "context/search-context";
import { fetchAPI } from "../../lib/api";

const PortfolioPage = ({ globalData, projects, portfoliopage }) => {
  const { seo, headerTitle, headerBackground } = portfoliopage;

  return (
    <MenuContextProvider>
      <SearchContextProvider>
        <Layout pageSeo={seo} globalData={globalData}>
          <PageBanner data={headerTitle} backgroundImage={headerBackground} />
          <PortfolioOne projects={projects} />
        </Layout>
      </SearchContextProvider>
    </MenuContextProvider>
  );
};

export async function getStaticProps() {

  const [globalData, projects, portfoliopage] = await Promise.all([
    fetchAPI("/global"),
    fetchAPI("/projects?_sort=date:DESC"),
    fetchAPI("/portfoliopage"),
  ])

  return {
    props: { globalData, projects, portfoliopage },
    revalidate: 1,
  }
}

export default PortfolioPage;
