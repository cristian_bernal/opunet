import React from "react";
import Layout from "@/components/layout";
import PageBanner from "@/components/page-banner";
import ProtfolioDetails from "@/components/portfolio/protfolio-details";
import MenuContextProvider from "context/menu-context";
import SearchContextProvider from "context/search-context";
import { fetchAPI } from "../../lib/api";
// import RelatedPortfolio from "@/components/portfolio/related-portfolio";

const PortfolioDetailsPage = ({ project, globalData, portfoliopage }) => {
  return (
    <MenuContextProvider>
      <SearchContextProvider>
        <Layout pageSeo={{
          metaTitle: project.name,
          metaDescription: project.description,
          shareImage: project.images[0]
        }}
          globalData={globalData}>

          <PageBanner data={{
            title: project.name,
            subTitle: "work showcase",
            text: portfoliopage.headerTitle.text
          }} backgroundImage={portfoliopage.headerBackground} />
          <ProtfolioDetails data={project} />
          {/*   <RelatedPortfolio /> */}
        </Layout>
      </SearchContextProvider>
    </MenuContextProvider>
  );
};

export async function getStaticPaths() {
  const projects = await fetchAPI("/projects")

  return {
    paths: projects.map((project) => ({
      params: {
        slug: project.slug,
      },
    })),
    fallback: false,
  }
}

export async function getStaticProps({ params }) {

  const projects = await fetchAPI(`/projects?slug=${params.slug}`)
  const globalData = await fetchAPI("/global")
  const portfoliopage = await fetchAPI("/portfoliopage")

  return {
    props: { project: projects[0], globalData, portfoliopage },
    revalidate: 1,
  }
}


export default PortfolioDetailsPage;
