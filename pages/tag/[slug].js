import React from "react";
import Layout from "@/components/layout";
import PageBanner from "@/components/page-banner";
import Articles from "@/components/blog/articles";
import SearchContextProvider from "context/search-context";
import MenuContextProvider from "context/menu-context";
import { fetchAPI } from "../../lib/api";

const Tag = ({ tag, globalData }) => {

    return (
        <MenuContextProvider>
            <SearchContextProvider>
                <Layout pageSeo={{
                    metaTitle: tag.name,
                    metaDescription: tag.seoDescription,
                    shareImage: tag.backgroundImage
                }} globalData={globalData}>

                    <PageBanner
                        data={{
                            title: tag.name,
                            subTitle: "Tags",
                            text: tag.seoDescription,

                        }} backgroundImage={tag.backgroundImage} />

                    <Articles
                        data={tag.articles} />

                </Layout>
            </SearchContextProvider>
        </MenuContextProvider>
    );
};

export async function getStaticPaths() {
    const categories = await fetchAPI("/tags")

    return {
        paths: categories.map((tag) => ({
            params: {
                slug: tag.slug,
            },
        })),
        fallback: false,
    }
}

export async function getStaticProps({ params }) {
    const tag = (await fetchAPI(`/tags?slug=${params.slug}`))[0]
    const globalData = await fetchAPI("/global")
    // const categories = await fetchAPI("/categories")

    return {
        props: { tag, globalData },
        revalidate: 1,
    }
}

export default Tag
