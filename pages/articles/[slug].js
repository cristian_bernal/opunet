import React from "react";
import Layout from "@/components/layout";
import PageBanner from "@/components/page-banner";
import BlogDetails from "@/components/blog/blog-details";
import MenuContextProvider from "context/menu-context";
import SearchContextProvider from "context/search-context";
import { fetchAPI } from "../../lib/api";

const BlogSinglePage = ({ article, globalData, categories, tags, allArticles }) => {

    return (
        <MenuContextProvider>
            <SearchContextProvider>
                <Layout
                    pageSeo={{
                        metaTitle: article.title,
                        metaDescription: article.description,
                        shareImage: article.image
                    }}
                    globalData={globalData}>

                    <PageBanner
                        data={{
                            title: article.title,
                            subTitle: "Details",
                            text: article.description
                        }}
                        backgroundImage={article.image} />

                    <BlogDetails data={{
                        article,
                        categories,
                        tags,
                        allArticles
                    }} />

                </Layout>
            </SearchContextProvider>
        </MenuContextProvider>
    );
};

export async function getStaticPaths() {
    const articles = await fetchAPI("/articles")

    return {
        paths: articles.map((article) => ({
            params: {
                slug: article.slug,
            },
        })),
        fallback: false,
    }
}


export async function getStaticProps({ params }) {

    const articles = await fetchAPI(`/articles?slug=${params.slug}`)
    const globalData = await fetchAPI("/global")
    const categories = await fetchAPI("/categories")
    const tags = await fetchAPI("/tags")
    const allArticles = await fetchAPI("/articles")
    // const allArticles = await fetchAPI("/articles?_sort=date:DESC&_limit=3")

    return {
        props: { article: articles[0], globalData, categories, tags, allArticles },
        revalidate: 1,
    }
}

export default BlogSinglePage;
