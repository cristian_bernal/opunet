import React from "react";
import Layout from "@/components/layout";
import PageBanner from "@/components/page-banner";
import Articles from "@/components/blog/articles";
import SearchContextProvider from "context/search-context";
import MenuContextProvider from "context/menu-context";
import { fetchAPI } from "../../lib/api";

const BlogPage = ({ globalData, articles, blogpage }) => {

    return (
        <MenuContextProvider>
            <SearchContextProvider>
                <Layout pageSeo={blogpage.seo} globalData={globalData}>

                    <PageBanner
                        data={blogpage.blogPageTitle} backgroundImage={blogpage.headerBackground} />

                    <Articles
                        data={articles} />

                </Layout>
            </SearchContextProvider>
        </MenuContextProvider>
    );
};

export async function getStaticProps() {

    const [globalData, articles, blogpage] = await Promise.all([
        fetchAPI("/global"),
        fetchAPI("/articles?_sort=date:DESC"),
        fetchAPI("/blog-page"),
    ])

    return {
        props: { globalData, articles, blogpage },
        revalidate: 1,
    }
}

export default BlogPage;
