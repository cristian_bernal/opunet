import qs from 'qs'
import { useRouter } from 'next/router'
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import Layout from "@/components/layout";
import PageBanner from "@/components/page-banner";
import Articles from "@/components/blog/articles";
import SearchContextProvider from "context/search-context";
import MenuContextProvider from "context/menu-context";
import { fetchAPI } from "../../lib/api";

const SearchResults = ({ globalData, articles, blogpage }) => {
    const router = useRouter()

    return (
        <MenuContextProvider>
            <SearchContextProvider>
                <Layout pageSeo={blogpage.seo} globalData={globalData}>

                    <PageBanner
                        data={{
                            title: `${router.query.term.length < 15 ? router.query.term : router.query.term.substring(0, 15) + "..."}`,
                            subTitle: "Search Results For",
                            text: ""
                        }} />

                    {articles.length < 1 ? (
                        <Container>
                            <Row
                                style={{
                                    minHeight: "60vh",
                                    alignItems: "center",
                                    backgroundColor: "#f4f4f4"
                                }}>
                                <Col lg={12} className="text-center">
                                    <h1 style={{ fontSize: "5rem" }}>
                                        Sorry, no results for "{router.query.term}"
                                    </h1>
                                </Col>
                            </Row>
                        </Container>

                    ) : (
                        <Articles data={articles} />
                    )}

                </Layout>
            </SearchContextProvider>
        </MenuContextProvider>
    );
};

export async function getServerSideProps({ query: { term } }) {

    const query = qs.stringify({
        _where: {
            _or: [
                { title_contains: term },
                { description_contains: term },
                { content_contains: term },
                { "category.name": term },
                { "author.name": term },
                { "tags.name": term },
            ],
        },
    })

    const [globalData, articles, blogpage] = await Promise.all([
        fetchAPI("/global"),
        fetchAPI(`/articles?${query}`),
        fetchAPI("/blog-page"),
    ])

    return {
        props: { globalData, articles, blogpage }
    }
}

export default SearchResults;
