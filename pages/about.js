import React from "react";
import Layout from "@/components/layout";
import PageBanner from "@/components/page-banner";
import TeamSection from "@/components/team/team-section";
import OurMission from "@/components/about/our-mision";
import SubscribeForm from "@/components/subscribe-form";
import TestimonialsCarousel from "@/components/about/testimonials-carousel";
import SearchContextProvider from "@/context/search-context";
import MenuContextProvider from "@/context/menu-context";
import AboutSection from "@/components/about/about-section";
import ClientCarousel from "@/components/about/client-carousel";
import { fetchAPI } from "../lib/api";

const AboutPage = ({ globalData, aboutPage, subscribers }) => {

  const {
    seo,
    headerTitle,
    headerBackground,
    section1Title,
    section1Images,
    section1Button,
    mission,
    vision,
    testimonials,
    ourTeamTitle,
    teamMember,
    ourClientsTitle,
    clientsLogos
  } = aboutPage

  return (
    <MenuContextProvider>
      <SearchContextProvider>
        <Layout pageSeo={seo} globalData={globalData}>
          <PageBanner data={headerTitle} backgroundImage={headerBackground} />
          <AboutSection data={{
            sectionTitle: section1Title,
            button: section1Button,
            gallery: section1Images
          }} />
          <OurMission data={{ mission, vision }} />
          <TestimonialsCarousel data={testimonials} />
          <TeamSection data={{ ourTeamTitle, teamMember }} />
          <ClientCarousel data={{ ourClientsTitle, clientsLogos }} />
          <SubscribeForm subscribers={subscribers} />
        </Layout>
      </SearchContextProvider>
    </MenuContextProvider>
  );
};

export async function getStaticProps() {

  const [globalData, aboutPage, subscribers] = await Promise.all([
    fetchAPI("/global"),
    fetchAPI("/aboutpage"),
    fetchAPI("/subscribers")
  ]);

  return {
    props: { globalData, aboutPage, subscribers },
    revalidate: 1,
  }
}

export default AboutPage;
