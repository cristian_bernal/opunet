import validator from "validator";
import { isEmpty } from "lodash";

const validateAndSanitizeContactForm = (data) => {
    let errors = {};
    let sanitizedData = {};

    /**
     * Set the firstName value equal to an empty string if user has not entered the firstName, otherwise the Validator.isEmpty() wont work down below.
     * Note that the isEmpty() here is our custom function defined in is-empty.js and
     * Validator.isEmpty() down below comes from validator library.
     * Similarly we do it for for the rest of the fields
     */
    data.firstName = !isEmpty(data.firstName) ? data.firstName : "";
    data.lastName = !isEmpty(data.lastName) ? data.lastName : "";
    data.phone = !isEmpty(data.phone) ? data.phone : "";
    data.email = !isEmpty(data.email) ? data.email : "";
    data.message = !isEmpty(data.message) ? data.message : "";
    /**
     * Checks for error if required is true
     * and adds Error and Sanitized data to the errors and sanitizedData object
     *
     * @param {String} fieldName Field name e.g. First name, last name
     * @param {String} errorContent Error Content to be used in showing error e.g. First Name, Last Name
     * @param {Integer} min Minimum characters required
     * @param {Integer} max Maximum characters required
     * @param {String} type Type e.g. email, phone etc.
     * @param {boolean} required Required if required is passed as false, it will not validate error and just do sanitization.
     */
    const addErrorAndSanitizedData = (
        fieldName,
        errorContent,
        min,
        max,
        type = "",
        required
    ) => {

        if (!validator.isLength(data[fieldName], { min, max })) {
            errors[fieldName] = `${errorContent} must be ${min} to ${max} characters`;
        }

        if ("email" === type && !validator.isEmail(data[fieldName])) {
            errors[fieldName] = `${errorContent} is not valid`;
        }

        if ("phone" === type && !validator.isMobilePhone(data[fieldName])) {
            errors[fieldName] = `${errorContent} is not valid`;
        }

        if (required && validator.isEmpty(data[fieldName])) {
            errors[fieldName] = `${errorContent} is required`;
        }

        // If no errors
        if (!errors[fieldName]) {
            sanitizedData[fieldName] = validator.trim(data[fieldName]);
            sanitizedData[fieldName] =
                "email" === type
                    ? validator.normalizeEmail(sanitizedData[fieldName])
                    : sanitizedData[fieldName];
            sanitizedData[fieldName] = validator.escape(sanitizedData[fieldName]);
        }
    };

    addErrorAndSanitizedData("firstName", "First name", 3, 35, "string", true);
    addErrorAndSanitizedData("lastName", "Last name", 3, 35, "string", true);
    addErrorAndSanitizedData("phone", "Phone number", 9, 15, "phone", true);
    addErrorAndSanitizedData("email", "Email", 8, 254, "email", true);
    addErrorAndSanitizedData("message", "Message", 30, 1000, "string", true);

    return {
        sanitizedData,
        errors,
        isValid: isEmpty(errors),
    };
};

export default validateAndSanitizeContactForm;
