import React from "react";
import Link from "next/link";

const CallToActionOne = ({ data }) => {
  const { title, text, button } = data
  return (
    <section className="commonSection call-to-action">
      <div className="container">
        <div className="row">
          <div className="col-lg-8 col-sm-12 col-md-7">
            <h2 className="sec_title dot white">{title}</h2>
            <p>{text}</p>
          </div>
          <div className="col-lg-4  col-sm-12 col-md-5 text-right">
            <Link href={button.url}>
              <a className="common_btn">
                <span>{button.name}</span>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default CallToActionOne;
