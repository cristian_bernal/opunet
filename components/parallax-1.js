import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import Image from "next/image"
// import { ParallaxOneData } from "@/data";

const ParallaxOne = ({ data }) => {
  const { icon, bigText1, bigText2, smallText, backgroundImage } = data;
  return (
    <section
      className="commonSection testimonial"
      style={{ backgroundImage: `url(${backgroundImage.url})` }}
    >
      <Container>
        <Row>
          <Col lg={{ span: 10, offset: 1 }} sm={12} className="text-center">
            <div className="testimonial_content">
              <div className="testi_icon">
                {null !== icon ? (
                  <Image
                    loading="eager"
                    priority={true}
                    src={icon.url}
                    width="72"
                    height="72"
                  />
                ) : (
                  <i className="mei-team"></i>
                )}
              </div>
              <h2>
                {bigText1}
                <span> {bigText2}</span>
              </h2>
              <p>{smallText}</p>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default ParallaxOne;
