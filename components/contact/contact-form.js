import React, { useState } from "react";
import validateAndSanitizeContactForm from "../../utils/validator/contact-form";
import { API_URL } from "@/config";
import Error from "./error"

const ContactForm = ({ data }) => {
  const { subTitle, title, text } = data;

  const initialState = {
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    message: "",
    errors: null,
  };
  const [input, setInput] = useState(initialState);
  const [successMessage, setSuccessMessage] = useState(null)
  const [errorMessage, setErrorMessage] = useState(null)
  const [isLoading, setIsLoading] = useState(false)

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    const result = validateAndSanitizeContactForm(input);
    if (!result.isValid) {
      setInput({ ...input, errors: result.errors });
      return;
    }
    setIsLoading(true)
    const res = await fetch(`${API_URL}/contact-forms/custom`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(result.sanitizedData)
    })
    const data = await res.json()

    if (res.ok) {
      setSuccessMessage(`Thanks for contacting us ${data.firstName}. We will reply to you as soon as possible`)
      setInput(initialState);
      setTimeout(() => setSuccessMessage(null), 5000)
      setIsLoading(false)
    } else {
      setIsLoading(false)
      setErrorMessage("Something went wrong. Please, try again")
      setTimeout(() => setErrorMessage(null), 5000)
    }
  };

  const handleOnChange = (event) => {
    const newState = { ...input, [event.target.name]: event.target.value };
    setInput(newState);
  };

  return (
    <section className="commonSection ContactPage">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 text-center">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 text-center">
                  <h4 className="sub_title">{subTitle}</h4>
                  <h2 className="sec_title">{title}</h2>
                  <p className="sec_desc">{text}</p>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-8 offset-lg-2 col-sm-12 col-md-10 offset-md-1">
                  <form
                    onSubmit={handleFormSubmit}
                    className="contactForm"
                    id="contactForm"
                  >
                    <div className="row">
                      <div className="col-lg-6 col-sm-6">
                        <input
                          onChange={handleOnChange}
                          value={input.firstName}
                          className="input-form required"
                          type="text"
                          name="firstName"
                          id="f_name"
                          placeholder="First Name"
                        />
                        <Error errors={input.errors} fieldName={"firstName"} />
                      </div>
                      <div className="col-lg-6 col-sm-6">
                        <input
                          onChange={handleOnChange}
                          value={input.lastName}
                          className="input-form required"
                          type="text"
                          name="lastName"
                          id="l_name"
                          placeholder="Last Name"
                        />
                        <Error errors={input.errors} fieldName={"lastName"} />
                      </div>
                      <div className="col-lg-6 col-sm-6">
                        <input
                          onChange={handleOnChange}
                          value={input.email}
                          className="input-form required"
                          type="email"
                          name="email"
                          id="email"
                          placeholder="Email Address"
                        />
                        <Error errors={input.errors} fieldName={"email"} />
                      </div>
                      <div className="col-lg-6 col-sm-6">
                        <input
                          onChange={handleOnChange}
                          value={input.phone}
                          className="input-form"
                          type="text"
                          name="phone"
                          id="phone"
                          placeholder="Phone Number"
                        />
                        <Error errors={input.errors} fieldName={"phone"} />
                      </div>
                      <div className="col-lg-12 col-sm-12">
                        <textarea
                          onChange={handleOnChange}
                          value={input.message}
                          className="input-form required"
                          name="message"
                          id="con_message"
                          placeholder="Write Message"
                        ></textarea>
                        <Error errors={input.errors} fieldName={"message"} />
                      </div>
                    </div>
                    {isLoading && (<div className="sending">Sending...</div>)}
                    {successMessage && (<div className="afterSubmitMessage afterSubmitMessage--success">{successMessage}</div>)}
                    {errorMessage && (<div className="afterSubmitMessage afterSubmitMessage--error">{errorMessage}</div>)}
                    <button
                      className="common_btn red_bg"
                      type="submit"
                      id="con_submit"
                    >
                      <span>Send Message</span>
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
          {/* <div className="col-lg-6 text-center">
            <div className="container">
              <div className="row">
                <div className="col-lg-12 text-center">
                  <h4 className="sub_title">{subTitle}</h4>
                  <h2 className="sec_title">{title}</h2>
                  <p className="sec_desc">{description}</p>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-8 offset-lg-2 col-sm-12 col-md-10 offset-md-1">
                  <form
                    action="#"
                    method="post"
                    className="contactFrom"
                    id="contactForm"
                  >
                    <div className="row">
                      <div className="col-lg-6 col-sm-6">
                        <input
                          className="input-form required"
                          type="text"
                          name="f_name"
                          id="f_name"
                          placeholder="First Name"
                        />
                      </div>
                      <div className="col-lg-6 col-sm-6">
                        <input
                          className="input-form required"
                          type="text"
                          name="l_name"
                          id="l_name"
                          placeholder="Last Name"
                        />
                      </div>
                      <div className="col-lg-6 col-sm-6">
                        <input
                          className="input-form required"
                          type="email"
                          name="email"
                          id="email"
                          placeholder="Email Address"
                        />
                      </div>
                      <div className="col-lg-6 col-sm-6">
                        <input
                          className="input-form"
                          type="text"
                          name="phone"
                          id="phone"
                          placeholder="Phone Number"
                        />
                      </div>
                      <div className="col-lg-12 col-sm-12">
                        <textarea
                          className="input-form required"
                          name="con_message"
                          id="con_message"
                          placeholder="Write Message"
                        ></textarea>
                      </div>
                    </div>
                    <button
                      className="common_btn red_bg"
                      type="submit"
                      id="con_submit"
                    >
                      <span>Send Message</span>
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div> */}
        </div>
      </div>
    </section>
  );
};

export default ContactForm;
