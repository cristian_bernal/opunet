import React, { useContext, Fragment, useRef } from "react";
import Link from "next/link";
import Image from "next/image";
import { MenuContext } from "@/context/menu-context";

const PopupMenu = ({ data }) => {
  const { links, logo, footer } = data
  const { menuStatus, updateMenuStatus } = useContext(MenuContext);
  const menuEl = useRef(null);
  const handleMenuClick = (e) => {
    e.preventDefault();
    updateMenuStatus(!menuStatus);
  };
  return (
    <div className="show-overlay-nav">
      <div className="popup popup__menu">
        <a
          href=""
          id="close-popup"
          onClick={handleMenuClick}
          className="close-popup"
        ></a>
        <div className="container mobileContainer">
          <div className="row">
            <div className="col-lg-12 text-left">
              <div className="logo2">
                <Link href="/">
                  <a>
                    <Image
                      loading="eager"
                      priority={true}
                      src={logo.url}
                      width="173"
                      height="35" />
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <div className="popup-inner">
                <nav
                  className="popup-menu dl-menu__wrap text-center"
                  ref={menuEl}
                >
                  <ul>
                    {links.map((link, index) => {
                      return (
                        <li
                          key={index}
                          className={`${undefined !== link.subItems
                            ? "menu-item-has-children"
                            : ""
                            }`}
                        >
                          <Link href={link.url}>
                            <a>{link.name}</a>
                          </Link>
                          {undefined !== link.subItems ? (
                            <Fragment>
                              <button
                                onClick={(e) => {
                                  menuEl.current
                                    .querySelectorAll(".sub-menu")
                                    .forEach((item) => {
                                      item.classList.remove("show");
                                    });

                                  let clickedItem = e.currentTarget.parentNode;
                                  clickedItem
                                    .querySelector(".sub-menu")
                                    .classList.toggle("show");
                                }}
                              >
                                <i className="fa fa-angle-down"></i>
                              </button>
                              <ul className="sub-menu">
                                {link.subItems.map((subLink, index) => (
                                  <li key={index}>
                                    <Link href={subLink.url}>
                                      <a>{subLink.name}</a>
                                    </Link>
                                  </li>
                                ))}
                              </ul>
                            </Fragment>
                          ) : null}
                        </li>
                      );
                    })}
                  </ul>
                </nav>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6 col-sm-12 col-xs-12 text-center text-md-left">
              <ul className="footer__contacts">
                <li>Phone:  {footer.contact.phone}</li>
                <li>Email: <a href={`mailto: ${footer.contact.email}`} >{footer.contact.email}</a></li>
                <li>
                  Address: {footer.contact.address}
                </li>
              </ul>
            </div>
            <div className="col-lg-6 col-sm-12">
              <div className="popUp_social text-center text-md-right">
                <ul>
                  <li>
                    <a href={footer.socialLinks.linkedin}>
                      <i className="fa fa-linkedin"></i>LinkedIn
                    </a>
                  </li>
                  <li>
                    <a href={footer.socialLinks.facebook}>
                      <i className="fa fa-facebook-square"></i>Facebook
                    </a>
                  </li>
                  <li>
                    <a href={footer.socialLinks.youtube}>
                      <i className="fa fa-youtube-play"></i>Youtube
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PopupMenu;
