import React, { useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { API_URL } from "@/config";


const SubscribeForm = ({ subscribers }) => {
  const [email, setEmail] = useState("")
  const [message, setMessage] = useState(null)
  const [success, setSuccess] = useState(false)

  const isValidEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  const emailIsNotUnique = subscribers.some(subscriber => subscriber.email === email)

  const onSubmit = async (e) => {
    e.preventDefault()
    if (isValidEmail(email)) {
      if (emailIsNotUnique) {
        setMessage("Email already subscribed")
        return
      }

      const res = await fetch(`${API_URL}/subscribers`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ email })
      })
      // const data = await res.json()

      if (res.ok) {
        setMessage("Thanks for subscribing to us")
        setEmail("")
        setSuccess(true)
      } else {
        setMessage("Something went wrong. Please, try again")
      }
    } else {
      if (email === "") {
        setMessage("Please, write down your email")
      } else {
        setMessage("Please, provide a valid email")
      }
    }
  }

  return (
    <section className="commonSection subscribe">
      <Container>
        <Row>
          <Col lg={4}>
            <h4 className="sub_title">don’t miss out our latest updates</h4>
            <h2 className="sec_title">Subscribe us</h2>
          </Col>
          <Col lg={8}>
            <form onSubmit={onSubmit} method="post" className="subscribefrom">
              <input
                style={{ display: `${success ? "none" : ""}` }}
                type="text"
                placeholder="Enter your email"
                name="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)} />
              {message && (
                <p className={`${!success ? "message-error" : "message-success"}`}>{message}</p>
              )}
              <button
                className={`common_btn red_bg ${success ? "common_btn--disabled" : ""}`}
                type="submit" name="submit">
                <span>Subscribe now</span>
              </button>
            </form>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default SubscribeForm;
