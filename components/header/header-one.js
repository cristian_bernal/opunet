import React, { useState, useEffect, useContext } from "react";
import Image from "next/image";
import { Col, Container, Row } from "react-bootstrap";
import { SearchContext } from "@/context/search-context";
import { MenuContext } from "@/context/menu-context";
import Link from "next/link";

const HeaderOne = ({ data }) => {
  const { links, logo } = data
  const [sticky, setSticky] = useState(false);
  const { searchStatus, updateSearchStatus } = useContext(SearchContext);
  const { menuStatus, updateMenuStatus } = useContext(MenuContext);
  const handleSearchClick = (e) => {
    e.preventDefault();
    updateSearchStatus(!searchStatus);
  };
  const handleMenuClick = (e) => {
    e.preventDefault();
    updateMenuStatus(!menuStatus);
  };

  const handleScroll = () => {
    if (window.scrollY > 70) {
      setSticky(true);
    } else if (window.scrollY < 70) {
      setSticky(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [sticky]);

  return (
    <header
      className={`header_01 ${true === sticky ? "fixedHeader animated flipInX" : null
        }`}
      id="header"
    >
      <Container fluid>
        <Row className="justify-content-between">
          <Col className="col-8" lg={3} md={8} >
            <div className="logo">
              <Link href="/">
                <a>
                  <Image
                    loading="eager"
                    priority={true}
                    src={logo.url}
                    width="173"
                    height="35" />
                </a>
              </Link>
            </div>
          </Col>
          <Col lg={6} className="d-none d-lg-block ">
            <nav className="mainmenu text-center">
              <ul>
                {links.map((link, index) => {
                  return (
                    <li
                      key={index}
                      className={`${undefined !== link.subItems
                        ? "menu-item-has-children"
                        : ""
                        }`}
                    >
                      <Link href={link.url}>
                        <a>{link.name}</a>
                      </Link>
                      {undefined !== link.subItems ? (
                        <ul className="sub-menu">
                          {link.subItems.map((subLink, index) => (
                            <li key={index}>
                              <Link href={subLink.url}>
                                <a>{subLink.name}</a>
                              </Link>
                            </li>
                          ))}
                        </ul>
                      ) : null}
                    </li>
                  );
                })}
              </ul>
            </nav>
          </Col>
          <Col lg={3} md={4} className="col-4">
            <div className="navigator text-right">
              <a
                className="search searchToggler"
                href="#"
                onClick={handleSearchClick}
              >
                <i className="mei-magnifying-glass"></i>
              </a>
              <a
                href="#"
                className="menu mobilemenu d-none d-md-none d-lg-none"
              >
                <i className="mei-menu"></i>
              </a>
              <a
                id="open-overlay-nav"
                className="menu hamburger"
                onClick={handleMenuClick}
                href="#"
              >
                <i className="mei-menu"></i>
              </a>
            </div>
          </Col>
        </Row>
      </Container>
    </header>
  );
};

export default HeaderOne;
