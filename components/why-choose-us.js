import React, { useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import SectionTitle from "@/components/section-title";
import Lines2 from "./Lines/Lines-2";

const WhyChooseUs = ({ data }) => {
  const [active, setActive] = useState(1);
  const { sectionTitle, tabsContent } = data;
  return (
    <section className="commonSection chooseUs">
      <Container>
        <Row>
          <Col lg={12} className="text-center">
            <SectionTitle data={sectionTitle} />
          </Col>
        </Row>
        <div id="tabs">
          <Row>
            <Col lg={12}>
              <ul className="chooseUs_title">
                {tabsContent.map(({ title }, index) => (
                  <li
                    key={index}
                    className={`${active === index ? "active" : " "}`}
                  >
                    <a
                      href="#"
                      onClick={(e) => {
                        e.preventDefault();
                        setActive(index);
                      }}
                    >
                      {title}
                    </a>
                  </li>
                ))}
              </ul>
            </Col>
          </Row>
          <div className="tab-content">
            {tabsContent.map((post, index) => {
              return index === active ? (
                <div
                  className="tab-pane fade show active animated fadeIn"
                  id={`tes_tab_${index}`}
                  key={index}
                >
                  <Row
                    className={`${0 === index % 2 ? " " : "flex-lg-row-reverse"
                      }`}
                  >
                    <Col lg={7}>
                      <div className="wh_choose">
                        <p>{post.text}</p>
                        <ul>
                          {post.features.map((item, index) => (
                            <li key={index}>
                              <i className="fa fa-circle"></i>
                              <p>{item.feature}</p>
                            </li>
                          ))}
                        </ul>
                      </div>
                    </Col>
                    <Col lg={5}>
                      <div className="chose_img">
                        <img src={post.image.formats.small.url} alt={`chose_img-${index}`} />
                      </div>
                    </Col>
                  </Row>
                </div>
              ) : null;
            })}
          </div>
        </div>
      </Container>
      <Lines2
        path="M6 6V90H294V174"
        strokeDasharray="456"
        svgSize={{
          width: "300",
          height: "180",
          viewBox: "0 0 300 180"
        }}
        circlePosition={{
          x: "6", y: "6"
        }}
        id="line-chooseUs"
      />
    </section>
  );
};

export default WhyChooseUs;
