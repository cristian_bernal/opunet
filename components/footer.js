import React from "react";
import Image from "next/image";

const Footer = ({ data, logo }) => {

  const { text, socialLinks, contact } = data

  return (
    <footer className="footer_1">
      <div className="container">
        <div className="row">
          <div className="col-lg-5 col-sm-6 col-md-5">
            <aside className="widget aboutwidget">
              <a href="/">
                <Image
                  loading="eager"
                  priority={true}
                  src={logo.url}
                  width="173"
                  height="35" />
              </a>
              <p>
                {text}
              </p>
            </aside>
          </div>
          <div className="col-lg-4 col-sm-4 col-md-4">
            <aside className="widget contact_widgets">
              <h3 className="widget_title">contact</h3>
              <p>
                {contact.address}
              </p>
              <p>P: {contact.phone}</p>
              <p>
                E: <a href={`mailto: ${contact.email}`} >{contact.email}</a>
              </p>
            </aside>
          </div>
          <div className="col-lg-3 col-sm-2 col-md-3">
            <aside className="widget social_widget">
              <h3 className="widget_title">social</h3>
              <ul>
                <li>
                  <a href={socialLinks.linkedin}>
                    <i className="fa fa-linkedin"></i>LinkedIn
                  </a>
                </li>
                <li>
                  <a href={socialLinks.facebook}>
                    <i className="fa fa-facebook-square"></i>Facebook
                  </a>
                </li>
                <li>
                  <a href={socialLinks.youtube}>
                    <i className="fa fa-youtube-play"></i>Youtube
                  </a>
                </li>
              </ul>
            </aside>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12 col-sm-12 text-center">
            <div className="copyright">
              © copyright Opunet {new Date().getFullYear()}, All rights reserved.
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
