import React from "react";
import Image from "next/image";

const TeamCard = ({ data }) => {
  const { photo, name, designation, links } = data;
  return (
    <div className="singleTM">
      <div className="tm_img">
        {photo.formats !== null ? (
          <Image
            loading="eager"
            priority={true}
            src={photo.formats.small.url}
            layout="fill"
          />
        ) : (
          <Image
            loading="eager"
            priority={true}
            src={photo.url}
            layout="fill"
          />
        )}
        <div className="tm_overlay">
          <div className="team_social">
            {links.map(({ name, url }, index) => (
              <a key={index} href={url}>
                <span>{name}</span>
              </a>
            ))}
          </div>
        </div>
      </div>
      <div className="detail_TM">
        <h5>{name}</h5>
        <h6>{designation}</h6>
      </div>
    </div>
  );
};

export default TeamCard;
