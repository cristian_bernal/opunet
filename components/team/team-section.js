import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import SectionTitle from "@/components/section-title";
import TeamCard from "@/components/team/team-card";

const TeamSection = ({ data }) => {
  const { ourTeamTitle, teamMember } = data;

  return (
    <section className="commonSection team">
      <Container>
        <Row>
          <Col lg={12} className="text-center">
            <SectionTitle data={ourTeamTitle} />
          </Col>
        </Row>
        <Row>
          {teamMember.map((post, index) => (
            <Col key={index} md={3} xs={6}>
              <TeamCard data={post} />
            </Col>
          ))}
        </Row>
      </Container>
    </section>
  );
};

export default TeamSection;
