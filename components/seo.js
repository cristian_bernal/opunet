// import Head from "next/head"

const Seo = ({ seo, siteName }) => {

  const fullSeo = {
    ...seo,
    // Add title suffix
    metaTitle: `${seo.metaTitle} | ${siteName}`,
  }

  return (
    <>
      {/*  {fullSeo.favicon && (
        <link rel="shortcut icon" href={fullSeo.favicon.formats.thumbnail.url} />
      )} */}
      {fullSeo.metaTitle && (
        <>
          <title>{fullSeo.metaTitle}</title>
          <meta property="og:title" content={fullSeo.metaTitle} />
          <meta name="twitter:title" content={fullSeo.metaTitle} />
        </>
      )}
      {fullSeo.metaDescription && (
        <>
          <meta name="description" content={fullSeo.metaDescription} />
          <meta property="og:description" content={fullSeo.metaDescription} />
          <meta name="twitter:description" content={fullSeo.metaDescription} />
        </>
      )}
      {fullSeo.shareImage && (
        <>
          <meta property="og:image" content={fullSeo.shareImage.url} />
          <meta name="twitter:image" content={fullSeo.shareImage.url} />
          <meta name="image" content={fullSeo.shareImage.url} />
        </>
      )}
      {/* {fullSeo.article && <meta property="og:type" content="article" />} */}
      <meta name="twitter:card" content="summary_large_image" />
    </>
  )
}

export default Seo
