import React from "react";

const OldWayCard = ({ data }) => {
  const { title, texts, index } = data;
  return (
    <div className="old_way_card">
      <h3>{title}</h3>
      {texts.map(text => (
        <p key={text.id}>{text.text}</p>
      ))}
      {index !== 2 && (
        <i className="fa fa-arrow-circle-o-right"></i>
      )}
    </div>
  );
};

export default OldWayCard;
