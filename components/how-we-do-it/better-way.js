import React from "react";
import Image from "next/image";
import Link from "next/link";
import SectionTitle from "../section-title";
import dynamic from 'next/dynamic'
const ReactMarkdown = dynamic(
  () => import("react-markdown"),
  { ssr: false }
)

const BetterWay = ({ data }) => {
  const {
    betterWayTitle,
    betterWayImage,
    betterWayFeatures,
    betterWayContent,
    callToActionButton
  } = data

  const zeroPad = (num, numZeros) => {
    var n = Math.abs(num);
    var zeros = Math.max(0, numZeros - Math.floor(n).toString().length);
    var zeroString = Math.pow(10, zeros).toString().substr(1);
    if (num < 0) {
      zeroString = "-" + zeroString;
    }

    return zeroString + n;
  };
  return (
    <section className="commonSection better-way">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 col-sm-12  col-md-5">
            <div className="better-way__content">
              <SectionTitle data={betterWayTitle} />
              <div className="better-way__content__sub">
                <ReactMarkdown
                  children={betterWayContent}
                  allowDangerousHtml={true}
                />
              </div>
              {betterWayFeatures.map(({ title, texts }, index) => {
                return (
                  <div className="better-way__features" key={index}>
                    <div className="f_count">{zeroPad(index + 1, 2)}</div>
                    <h3>{title}</h3>
                    {texts.map(text => (
                      <p key={text.id}>{text.text}</p>
                    ))}
                  </div>
                );
              })}
            </div>
          </div>
          <div className="col-lg-6 col-sm-12 col-md-7 noPaddingRight">
            <div className="better-way__img">
              <Image
                loading="eager"
                priority={true}
                src={betterWayImage.url}
                width={betterWayImage.width}
                height={betterWayImage.height}
              />
              <Link href={callToActionButton.url}>
                <a className="common_btn red_bg">
                  <span>{callToActionButton.name}</span>
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default BetterWay;
