import React from "react";
import Image from "next/image";
import { Col, Container, Row } from "react-bootstrap";
import OldWayCard from "@/components/how-we-do-it/old-way-card";
import SectionTitle from "../section-title";
import dynamic from 'next/dynamic'
const ReactMarkdown = dynamic(
  () => import("react-markdown"),
  { ssr: false }
)

const OldWay = ({ data }) => {
  const {
    oldWayTitle,
    oldWaySubTitle,
    oldWayImage,
    oldWayFeatures,
    oldWayContent
  } = data
  return (
    <section className="commonSection how_we_do_it">
      <Container>
        <Row className="old_way">
          <Col lg={6}>
            <div className="old_way__img">
              <Image
                loading="eager"
                priority={true}
                src={oldWayImage.formats.medium.url}
                layout="fill"
              />
            </div>
          </Col>
          <Col lg={6}>
            <SectionTitle data={{
              title: oldWayTitle,
              subTitle: oldWaySubTitle,
              text: ""
            }} />
            <ReactMarkdown
              children={oldWayContent}
              allowDangerousHtml={true}
            />
          </Col>
        </Row>
        <Row>
          {oldWayFeatures.map((feature, index) => (
            <Col lg={4} md={12} key={feature.id}>
              <OldWayCard data={{ ...feature, index }} />
            </Col>
          ))}
        </Row>
      </Container>
    </section>
  );
};

export default OldWay;
