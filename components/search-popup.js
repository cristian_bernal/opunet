import React, { useContext, useState } from "react";
import { useRouter } from 'next/router'
import { SearchContext } from "@/context/search-context";

const SearchPopup = () => {
  const { searchStatus, updateSearchStatus } = useContext(SearchContext);
  const [term, setTerm] = useState('')

  const router = useRouter()

  const handleSubmit = (e) => {
    e.preventDefault();
    updateSearchStatus(!searchStatus);
    router.push(`/articles/search?term=${term}`)
    setTerm('')
  }

  const handleSearchClick = (e) => {
    e.preventDefault();
    updateSearchStatus(!searchStatus);
  };

  return (
    <div className="searchFixed popupBG animated fadeIn d-block">
      <div className="container-fluid">
        <span
          id="sfCloser"
          className="sfCloser"
          onClick={handleSearchClick}
        ></span>
        <div className="searchForms">
          <div className="container">
            <div className="row">
              <div className="col-lg-8 offset-lg-2">
                <form onSubmit={handleSubmit}>
                  <input
                    type="text"
                    value={term}
                    className="searchField"
                    placeholder="Search articles..."
                    onChange={(e) => setTerm(e.target.value)}
                  />
                  <button type="submit">
                    <i className="fa fa-search"></i>
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SearchPopup;
