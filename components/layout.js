import React, { Fragment, useContext, useState, useEffect } from "react";
import Head from "next/head";
import { SearchContext } from "@/context/search-context";
import { MenuContext } from "@/context/menu-context";
import SearchPopup from "@/components/search-popup";
import PopupMenu from "@/components/popup-menu";
import { Link as ScrollLink } from "react-scroll";
import HeaderOne from "./header/header-one";
import Footer from "./footer";
import Seo from "./seo";

const Layout = ({ globalData, pageSeo, children }) => {

  const { searchStatus } = useContext(SearchContext);
  const { menuStatus } = useContext(MenuContext);
  const [scrollTop, setScrollTop] = useState(false);

  const handleScrollTop = () => {
    if (window.scrollY > 70) {
      setScrollTop(true);
    } else if (window.scrollY < 70) {
      setScrollTop(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScrollTop);
    return () => {
      window.removeEventListener("scroll", handleScrollTop);
    };
  }, [scrollTop]);

  return (
    <Fragment>
      <Head>
        {globalData.favicon && (
          <link rel="shortcut icon" href={globalData.favicon.formats.thumbnail.url} />
        )}
        <Seo seo={pageSeo} siteName={globalData.siteName} />
      </Head>
      <div id="wrapper">
        <HeaderOne data={{ links: globalData.navLinks, logo: globalData.logo }} />
        {children}
        <Footer data={globalData.footer} logo={globalData.logo} />
      </div>
      {true === searchStatus ? <SearchPopup /> : null}
      {true === menuStatus ? <PopupMenu data={{
        links: globalData.navLinks,
        logo: globalData.logo,
        footer: globalData.footer
      }} /> : null}

      {scrollTop === true ? (
        <ScrollLink
          to="wrapper"
          smooth={true}
          duration={500}
          id="backToTop"
          className="scroll-to-top showit"
        >
          <i className="fa fa-angle-double-up"></i>
        </ScrollLink>
      ) : null}
    </Fragment>
  );
};

export default Layout;
