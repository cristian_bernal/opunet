import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import BlogSidebar from "@/components/blog/blog-sidebar";
import SinglePostCard from "@/components/blog/single-post-card";

const BlogDetails = ({ data }) => {
  const { article, categories, tags, allArticles } = data

  return (
    <section className="commonSection blogDetails">
      <Container>
        <Row>
          <Col lg={8}>
            <SinglePostCard article={article} />
          </Col>
          <Col lg={4} className="sidebar">
            <BlogSidebar data={{ categories, tags, allArticles }} />
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default BlogDetails;
