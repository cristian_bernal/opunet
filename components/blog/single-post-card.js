import React, { Fragment } from "react";
import Link from "next/link"
import Moment from "react-moment"
import AuthorCard from "@/components/blog/author-card";
import dynamic from 'next/dynamic'
const ReactMarkdown = dynamic(
  () => import("react-markdown"),
  { ssr: false }
)
// import Image from "next/image";
// import Comments from "@/components/blog/comments";

const SinglePostCard = ({ article }) => {
  const { title, content, date, author, tags } = article

  return (
    <Fragment>
      <div className="single_blog">
        {/*  <div className="blog_thumb">
          <Image
            loading="eager"
            priority={true}
            src={image.formats.medium.url}
            layout="fill" />
        </div> */}
        <div className="blog_headings">
          <span className="blog_date">
            <Moment format="MMM DD YYYY">
              {date}
            </Moment>
          </span>
          <h2>{title}</h2>
          <p className="blog_metas">
            <span>{`By ${author.name}`}</span>
          </p>
        </div>
        <div className="blog_details">
          <ReactMarkdown
            children={content}
            allowDangerousHtml={true}
          />
        </div>
        <div className="blog_tagitems">
          <span>Tags:</span>
          {tags.map(tag => (
            <Link key={tag.id} as={`/tag/${tag.slug}`} href="/tag/[id]">
              <a>{tag.name}</a>
            </Link>
          ))}
        </div>
        <AuthorCard author={author} />
        {/*  <Comments /> */}
      </div>
    </Fragment>
  );
};

export default SinglePostCard;
