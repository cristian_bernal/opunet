import React from "react";
import Image from "next/image";
import Moment from "react-moment";
import Link from "next/link";

const BlogCard = ({ data }) => {
  const { image, title, slug, date } = data;

  return (
    <div className="latestBlogItem">
      <div className="lbi_thumb">
        <Image
          loading="eager"
          priority={true}
          src={image.formats.small.url}
          layout="fill"
        />
      </div>
      <div className="lbi_details">
        <Link as={`/articles/${slug}`} href="/articles/[id]">
          <p className="lbid_date">
            <Moment format="MMM DD YYYY">
              {date}
            </Moment>
          </p>
        </Link>
        <h2>
          <Link as={`/articles/${slug}`} href="/articles/[id]">
            <a>{title}</a>
          </Link>
        </h2>
        <Link as={`/articles/${slug}`} href="/articles/[id]">
          <a className="learnM">Learn More</a>
        </Link>
      </div>
    </div>
  );
};

export default BlogCard;
