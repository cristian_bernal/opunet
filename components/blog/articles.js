import React, { useState, useEffect } from "react";
import Pagination from "react-js-pagination";
import { Container, Row, Col } from "react-bootstrap";
import BlogCard from "@/components/blog/blog-card";

const Articles = ({ data }) => {

  const articles = data;
  const [activePage, setActivePage] = useState(1);
  const [currentArticles, setCurrentArticles] = useState(null);
  const articlesPerPage = 6;
  const pageRange = 5;

  /**
   * Whenever the articles length changes,
   * which means the Product component is re-rendered because
   * its parent changed the 'articles' value ( because search was done, by user, so new product results ),
   * then do the following :
   * 1. Set the active page to 1 ( refresh current page to 1 ),
   * 2. Update the articles to be displayed
   *
   */
  useEffect(() => {
    const activePage = 1;
    setActivePage(activePage);

    setArticlesToBeDisplayed(
      activePage * articlesPerPage
    );
  }, [articles.length]);

  useEffect(() => {
    setArticlesToBeDisplayed(activePage * articlesPerPage);
  }, [activePage]);

  const setArticlesToBeDisplayed = (lastArticleIndex) => {
    const indexOfLastArticle = lastArticleIndex; // e.g. 6
    const indexOfFirstArticle = indexOfLastArticle - articlesPerPage;

    // Get all the articles from index of first product, to index of last product
    const currentArticlesData = articles.slice(
      indexOfFirstArticle,
      indexOfLastArticle
    ); // e.g. articles from index 0 to 6 ( 6 items ).
    setCurrentArticles(currentArticlesData);
  };

  const handlePageChange = (pageNumber) => {
    setActivePage(pageNumber);
  };

  if (null === currentArticles) {
    return null;
  }

  return (
    <section className="commonSection blogPage">
      <Container>
        <Row>
          {currentArticles.map((post, index) => (
            <Col lg={4} sm={12} md={6} key={index}>
              <BlogCard data={post} />
            </Col>
          ))}
        </Row>
        <Pagination
          activePage={activePage}
          itemsCountPerPage={articlesPerPage}
          totalItemsCount={articles.length}
          pageRangeDisplayed={pageRange}
          onChange={handlePageChange}
          itemClass="page-item"
          linkClass="page-link"
          itemClassFirst="first"
          itemClassPrev="prev"
          itemClassNext="next"
          itemClassLast="last"
        />
      </Container>
    </section>
  );
};

export default Articles;
