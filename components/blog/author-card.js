import React from "react";
import Image from "next/image";
import Link from "next/link";

const AuthorCard = ({ author }) => {
  return (
    <div className="post_author">
      <div className="post_author__img">
        <Image
          loading="eager"
          priority={true}
          src={author.picture.formats.small.url}
          layout="fill" />
      </div>
      <h3>
        <p >{author.name}</p>
      </h3>
      <p>
        {author.bio}
      </p>
      <Link as={`/writers/${author.slug}`} href="/writers/[id]">
        <a>View all posts</a>
      </Link>
    </div>
  );
};

export default AuthorCard;
