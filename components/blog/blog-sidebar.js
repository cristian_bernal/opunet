import React, { Fragment, useEffect, useState, useMemo } from "react";
import Link from "next/link"
import Image from "next/image";

const BlogSidebar = ({ data }) => {
  const { categories, tags, allArticles } = data
  const [latestArticles, setLatestArticles] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");


  useEffect(() => {
    setLatestArticles(allArticles.sort((a, b) =>
      b.date.localeCompare(a.date)).slice(0, 3))
  }, [allArticles])


  const filteredArticles = useMemo(() => {
    return allArticles.filter((article) => {
      const searchResults = article.title
        .toLowerCase()
        .includes(searchTerm.toLowerCase()) ||
        article.description
          .toLowerCase()
          .includes(searchTerm.toLowerCase()) ||
        article.content
          .toLowerCase()
          .includes(searchTerm.toLowerCase()) ||
        article.category.name
          .toLowerCase()
          .includes(searchTerm.toLowerCase()) ||
        article.author.name
          .toLowerCase()
          .includes(searchTerm.toLowerCase())
      /*  article.tags.name
       .toLowerCase()
       .includes(searchTerm.toLowerCase()) */
      return (
        searchResults < 20 ? searchResults : searchResults.slice(0, 20)
      );
    });
  }, [searchTerm, allArticles]);


  return (
    <Fragment>
      <aside className="widget search-widget">
        <form className="searchform">
          <input onChange={(e) => setSearchTerm(e.target.value)} type="search" placeholder="Search here..." />
        </form>
      </aside>
      <aside className="widget recent_posts">
        <h3 className="widget_title">{searchTerm === "" ? "Latest Posts" : "Search Results"}</h3>
        <div className="meipaly_post_widget">
          {searchTerm === "" ? (
            latestArticles.map(({ title, image, slug }, index) => (
              <div className="mpw_item" key={index}>
                <div className="mpw_item__img">
                  <Image
                    priority={true}
                    src={image.formats.thumbnail.url}
                    layout="fill" />
                </div>
                <Link as={`/articles/${slug}`} href="/articles/[id]">
                  <a>{title}</a>
                </Link>
              </div>
            ))
          ) : (
            <>
              {filteredArticles.map(({ title, image, slug }, index) => (
                <div className="mpw_item" key={index}>
                  <div className="mpw_item__img">
                    <Image
                      priority={true}
                      src={image.formats.thumbnail.url}
                      layout="fill" />
                  </div>
                  <Link as={`/articles/${slug}`} href="/articles/[id]">
                    <a>{title}</a>
                  </Link>
                </div>
              ))}
              {filteredArticles.length === 0 && (
                <h4>Sorry, no results for "{searchTerm}"</h4>
              )}
            </>
          )}
        </div>
      </aside>
      <aside className="widget categories">
        <h3 className="widget_title">Categories</h3>
        <div className="categorie_widget">
          <ul>
            {categories.map(category => (
              <li key={category.id}>
                <Link as={`/category/${category.slug}`} href="/category/[id]">
                  <a className="uk-link-reset">{category.name}</a>
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </aside>
      <aside className="widget">
        <h3 className="widget_title">Tags:</h3>
        <div className="tagcloude_widget">
          {tags.map(tag => (
            <Link key={tag.id} as={`/tag/${tag.slug}`} href="/tag/[id]">
              <a>{tag.name}</a>
            </Link>
          ))}
        </div>
      </aside>
    </Fragment>
  );
};

export default BlogSidebar;
