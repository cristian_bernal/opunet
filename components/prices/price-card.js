import React from 'react'
// import { Container, Row, Col } from "react-bootstrap";

const PriceCard = ({ data }) => {
    const { planName, price, button, features, previousPlanName } = data
    return (
        <div className="price_card text-center" >
            <h3>{planName}</h3>
            <p className="price"> <span>${price}</span></p>
            {previousPlanName && (
                <div className="previous_plan">
                    <p>Everything provided in <span>{previousPlanName}</span> plan</p>
                    <i className="fa fa-plus"></i>
                </div>
            )}
            <ul className="price_card__features">
                {features.map((item, idx) => (
                    <li key={idx}>
                        <i className="fa fa-check-circle-o" aria-hidden="true"></i>
                        {item.feature}
                    </li>
                )
                )}
            </ul>
            <a className="price_card__btn" href={button.url}>{button.name}</a>
        </div>
    )
}

export default PriceCard
