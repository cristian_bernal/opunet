import React from 'react'
import { Row, Col, Container } from "react-bootstrap";
import SectionTitle from '../section-title';

const EcommercePlan = ({ data }) => {
    const { ecommercePlanTitle, ecommercePlanCard, phoneNumber } = data
    const { price, text, button } = ecommercePlanCard
    return (
        <div className="commonSection ecommerce_plan">
            <Container>
                <Row>
                    <Col lg={6} className="text-center">
                        <SectionTitle data={ecommercePlanTitle} />
                    </Col>
                    <Col lg={6} className="text-center" >
                        <div className="price_card">
                            <p className="price">From <span>${price}</span></p>
                            <span>
                                {text}
                            </span>
                            <a className="price_card__btn" href={button.url}>{button.name}</a>
                        </div>
                    </Col>
                    <Col lg={6} className="text-center ecommerce_plan__contact ">
                        <div className="info_wrapper">
                            <i className="fa fa-phone" aria-hidden="true"></i>
                            <p>
                                Call anytime
                                <br />
                                <a href={`tel:${phoneNumber}`}>{phoneNumber}</a>
                            </p>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default EcommercePlan
