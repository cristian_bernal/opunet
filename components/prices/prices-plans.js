import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import PriceCard from "./price-card";
import EcommercePlan from "./ecommerce-plan";
import Lines2 from "../Lines/Lines-2";

const PricingPlans = ({ data }) => {
    const { sectionTitle, plans, ecommercePlan } = data;
    const { title, subTitle, text } = sectionTitle

    return (
        <>
            <section className="pricing_plans commonSection">
                <Container>
                    <Row>
                        <Col md={12} className="text-center">
                            <h4 className="sub_title">{subTitle}</h4>
                            <h2 className="sec_title dot">{title}</h2>
                            <p className="sec_desc color_aaa">{text}</p>
                        </Col>
                    </Row>
                    <Row>
                        {plans.map((plan, index) => {
                            if (plans[0] === plan) {
                                return (
                                    <Col lg={4} md={6} sm={12} key={plan.id}>
                                        <PriceCard data={plan} />
                                    </Col>
                                )
                            } else {
                                return (
                                    <Col lg={4} md={6} sm={12} key={plan.id}>
                                        <PriceCard
                                            data={{
                                                ...plan,
                                                previousPlanName: plans[index - 1].planName
                                            }}
                                        />
                                    </Col>
                                )
                            }
                        })}
                    </Row>
                </Container>
                <Lines2
                    path="M274 6V90H6V174"
                    strokeDasharray="436"
                    svgSize={{
                        width: "280",
                        height: "180",
                        viewBox: "0 0 280 180"
                    }}
                    circlePosition={{
                        x: "274", y: "6"
                    }}
                    id="line-pricing_plans"
                />
            </section>
            <EcommercePlan data={ecommercePlan} />
        </>
    );
};

export default PricingPlans;
