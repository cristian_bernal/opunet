import React from "react";
import Image from "next/image";
import Link from "next/link";


const PortfolioCard = ({ data }) => {
  const { images, name, liveDemo, slug } = data;
  return (
    <div className={`singlefolio`}>
      <div className="singlefolio__img">
        {images.length && (
          <Image
            loading="eager"
            priority={true}
            src={images[0].url}
            layout="fill"
          />
        )}
      </div>
      <div className="folioHover">
        <a className="cate" href={liveDemo.url}>
          {liveDemo.name}
        </a>
        <Link as={`/portfolio/${slug}`} href="/portfolio/[id]">
          <a className="cate">Learn More</a>
        </Link>
        <h3>
          {name}
        </h3>
      </div>
    </div>
  );
};

export default PortfolioCard;
