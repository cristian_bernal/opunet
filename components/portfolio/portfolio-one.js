import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Pagination from "react-js-pagination";
import PortfolioCard from "@/components/portfolio/portfolio-card";

const PortfolioOne = ({ projects }) => {
  const [activePage, setActivePage] = useState(1);
  const [currentProjects, setCurrentProjects] = useState(null);
  const projectsPerPage = 6;
  const pageRange = 5;

  useEffect(() => {
    const activePage = 1;
    setActivePage(activePage);

    setProjectsToBeDisplayed(
      activePage * projectsPerPage
    );
  }, [projects.length]);

  useEffect(() => {
    setProjectsToBeDisplayed(activePage * projectsPerPage);
  }, [activePage]);


  const setProjectsToBeDisplayed = (lastProjectIndex) => {
    const indexOfLastProject = lastProjectIndex; // e.g. 6
    const indexOfFirstArticle = indexOfLastProject - projectsPerPage;

    // Get all the projects from index of first product, to index of last product
    const currentProjectsData = projects.slice(
      indexOfFirstArticle,
      indexOfLastProject
    ); // e.g. projects from index 0 to 6 ( 6 items ).
    setCurrentProjects(currentProjectsData);
  };

  const handlePageChange = (pageNumber) => {
    setActivePage(pageNumber);
  };

  if (null === currentProjects) {
    return null;
  }

  return (
    <section className="commonSection porfolioPage">
      <Container>
        <Row id="Grid">
          <div className="custom">
            <Row>
              {projects.map((project) => (
                <Col lg={4} md={6} sm={12} key={project.id}>
                  <PortfolioCard data={project} />
                </Col>
              ))}
            </Row>
          </div>
        </Row>
        <Row>
          <Col lg={12} className="text-center">
            <Pagination
              activePage={activePage}
              itemsCountPerPage={projectsPerPage}
              totalItemsCount={projects.length}
              pageRangeDisplayed={pageRange}
              onChange={handlePageChange}
              itemClass="page-item"
              linkClass="page-link"
              itemClassFirst="first"
              itemClassPrev="prev"
              itemClassNext="next"
              itemClassLast="last"
            />
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default PortfolioOne;
