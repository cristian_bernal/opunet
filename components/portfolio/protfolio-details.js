import React from "react";
import dynamic from 'next/dynamic'
import { Container, Row, Col } from "react-bootstrap";
import Image from "next/image";
import Moment from "react-moment";
const ReactMarkdown = dynamic(
  () => import("react-markdown"),
  { ssr: false }
)

const ProtfolioDetails = ({ data }) => {

  const {
    images,
    name,
    description,
    client,
    date,
    links
  } = data;

  return (
    <section className="commonSection porfolioDetail">
      <Container>
        <Row>
          <Col lg={8} md={7} sm={12}>
            {images.map((image, index) => (
              <div className="portDetailThumb" key={index}>
                <Image
                  loading="eager"
                  priority={true}
                  src={image.url}
                  width={image.width}
                  height={image.height}
                  alt="portDetailThumb"
                />
              </div>
            ))}
          </Col>
          <Col lg={4} md={5} sm={12}>
            <div className="singlePortfoio_content">
              <h3>{name}</h3>
              <ReactMarkdown
                children={description}
                allowDangerousHtml={true}
              />
              {/*  <p>{description}</p> */}
            </div>
            <div className="singlePortfoio_content">
              <h4>Client:</h4>
              <p>{client}</p>
            </div>
            <div className="singlePortfoio_content">
              <h4>Date:</h4>
              <Moment format="MMM DD YYYY">
                <p>{date}</p>
              </Moment>
            </div>
            <div className="singlePortfoio_content">
              <h4>Follow:</h4>
              <ul>
                {links.map(({ name, url }, index) => (
                  <li key={index}>
                    <a href={url}>{name}</a>
                  </li>
                ))}
              </ul>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default ProtfolioDetails;
