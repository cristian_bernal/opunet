import React from "react";
import SectionTitle from "./section-title";

const PageBanner = ({ data, backgroundImage }) => {
  return (
    <section
      style={{ backgroundImage: `url(${backgroundImage ? backgroundImage.url : "/images/bg/8.jpg"})` }}
      className={`pageBanner ${backgroundImage ? "pageBanner--dark" : ""}`}>
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="banner_content text-center">
              <SectionTitle data={data} />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default PageBanner;
