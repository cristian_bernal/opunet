import React, { useRef, useEffect, useCallback } from "react"
import { gsap } from "gsap/dist/gsap"
import { ScrollTrigger } from "gsap/dist/ScrollTrigger"
import { MotionPathPlugin } from "gsap/dist/MotionPathPlugin"


const Lines = ({ path, isEven }) => {
  const pathRef = useRef(null)
  const circleRef = useRef(null)
  const svgRef = useRef(null)

  const initAnimation = useCallback(() => {
    gsap
      .timeline()
      .to(svgRef.current, { visibility: "visible", duration: 0.1 })
      .fromTo(
        pathRef.current,
        { strokeDashoffset: 810 },
        {
          scrollTrigger: {
            trigger: pathRef.current,
            toggleActions: "restart, pause, reverse, pause",
            scrub: 0.5,
            start: "top 70%",
            end: "top",
          },
          strokeDashoffset: 0,
        }
      )
      .to(circleRef.current, {
        scrollTrigger: {
          trigger: pathRef.current,
          toggleActions: "restart, pause, reverse, pause",
          scrub: 0.5,
          start: "top 70%",
          end: "top",
        },
        motionPath: {
          path: pathRef.current,
          align: pathRef.current,
          alignOrigin: [0.5, 0.5],
        },
      })
  }, [])

  useEffect(() => {
    if (typeof window !== "undefined") {
      gsap.registerPlugin(ScrollTrigger)
      gsap.registerPlugin(MotionPathPlugin)
    }
    initAnimation()
  }, [])

  return (
    <svg
      id="lines"
      width="617"
      height="217"
      viewBox="0 0 617 217"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      ref={svgRef}
    >
      <path className="p-line p-line-01" d={path} />
      <path
        ref={pathRef}
        className={`p-line p-line-02`}
        strokeDasharray={810}
        d={path}
      />
      <path className="p-line p-line-03" d={path} />
      <>
        {isEven ? (
          <circle
            ref={circleRef}
            className="circle"
            cx="6"
            cy="7"
            r="5"
            fill="#D63E3D"
          />
        ) : (
          <circle
            ref={circleRef}
            className="circle"
            cx="611"
            cy="7"
            r="5"
            fill="#D63E3D"
          />
        )
        }
      </>
    </svg>
  )
}

export default Lines
