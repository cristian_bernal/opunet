import React, { useRef, useEffect, useCallback } from "react"
import { gsap } from "gsap/dist/gsap"
import { ScrollTrigger } from "gsap/dist/ScrollTrigger"
import { MotionPathPlugin } from "gsap/dist/MotionPathPlugin"


const Lines2 = ({ path, strokeDasharray, svgSize, circlePosition, id }) => {
    const pathRef = useRef(null)
    const circleRef = useRef(null)
    const svgRef = useRef(null)

    const initAnimation = useCallback(() => {
        gsap
            .timeline()
            .to(svgRef.current, { visibility: "visible", duration: 0.1 })
            .fromTo(
                pathRef.current,
                { strokeDashoffset: parseInt(strokeDasharray) },
                {
                    scrollTrigger: {
                        trigger: pathRef.current,
                        toggleActions: "restart, pause, reverse, pause",
                        scrub: 0.5,
                        start: "top 70%",
                        end: "top",
                    },
                    strokeDashoffset: 0,
                }
            )
            .to(circleRef.current, {
                scrollTrigger: {
                    trigger: pathRef.current,
                    toggleActions: "restart, pause, reverse, pause",
                    scrub: 0.5,
                    start: "top 70%",
                    end: "top",
                },
                motionPath: {
                    path: pathRef.current,
                    align: pathRef.current,
                    alignOrigin: [0.5, 0.5],
                },
            })
    }, [])

    useEffect(() => {
        if (typeof window !== "undefined") {
            gsap.registerPlugin(ScrollTrigger)
            gsap.registerPlugin(MotionPathPlugin)
        }
        initAnimation()
    }, [])

    return (
        <svg
            id={id}
            className="line-animation"
            fill="none"
            width={svgSize.width}
            height={svgSize.height}
            viewBox={svgSize.viewBox}
            xmlns="http://www.w3.org/2000/svg"
            ref={svgRef}
        >
            <path className="p-line p-line-01" d={path} />
            <path
                ref={pathRef}
                className={`p-line p-line-02`}
                strokeDasharray={strokeDasharray}
                d={path}
            />
            <path className="p-line p-line-03" d={path} />
            <circle
                ref={circleRef}
                className="circle"
                cx={circlePosition.x}
                cy={circlePosition.y}
                r={circlePosition.r ? circlePosition.r : "5"}
                fill="#D63E3D"
            />
        </svg>
    )
}

export default Lines2
