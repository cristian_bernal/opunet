import React, { useRef } from "react"
import Image from "next/image";
import Lines from "../Lines/Lines"
import SectionTitle from "@/components/section-title"


const Service = ({ service }) => {
  const { content, image, isEven } = service
  const { title, subTitle, text } = content
  const serviceContainerRef = useRef(null)
  const imageContainerRef = useRef(null)
  const articleContainerRef = useRef(null)

  return (
    <section ref={serviceContainerRef} className="section-service">
      <div
        ref={imageContainerRef}
        className={`service-img ${image.ext === ".svg" ? "service-img--icon" : "service-img--image"}`}>
        {/*  <div className="icon-wrapper">{icon}</div> */}
        <Image
          loading="eager"
          priority={true}
          src={image.url}
          layout="fill"
        />
      </div>
      <article ref={articleContainerRef} className="service-article">
        <SectionTitle data={{ title, subTitle, text }}
        />
      </article>
      <Lines
        path={isEven ? "M6 12V118.329H612V212" : "M611 12V118.329H5V212"}
        isEven={isEven}
      />
    </section>
  )
}

export default Service
