import React from "react";
import Link from "next/link";
import Image from "next/image"

const ServiceCardOne = ({ data }) => {

  const { title, text, icon, button, defaultIconName } = data;

  return (
    <div className="icon_box_2 text-center">
      <h3>{title}</h3>
      <p>{text}</p>
      {null !== icon ? (
        <div className="iconWrap iconWrap--changed">
          <Image
            loading="eager"
            priority={true}
            src={icon.url}
            width="72"
            height="72"
          />
        </div>
      ) : (
        <div className="iconWrap">
          <i className={defaultIconName}></i>
        </div>
      )}
      <Link href={button.url}>
        <a>{button.name}</a>
      </Link>
    </div>
  );
};

export default ServiceCardOne;
