import React from "react"
import { Container } from "react-bootstrap";
import Service from "./Service"

const Services = ({ services }) => {
  return (
    <section className="services-section">
      <Container>
        {services.map((service, index) => {
          return <Service key={service.id} service={{ isEven: index % 2 !== 0, ...service }} />
        })}
      </Container>
    </section>
  )
}


export default Services
