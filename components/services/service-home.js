import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import ServiceCardOne from "@/components/services/service-card-one";
import Lines2 from "../Lines/Lines-2";

const ServiceHome = ({ data }) => {
  const { sectionTitle, cards } = data
  const { title, subTitle, text } = sectionTitle;
  return (
    <section id="home-services" className="service_section_2 commonSection">
      <Container>
        <Row>
          <Col md={12} className="text-center">
            <h4 className="sub_title red_color">{subTitle}</h4>
            <h2 className="sec_title dot white">{title}</h2>
            <p className="sec_desc color_aaa">{text}</p>
          </Col>
        </Row>
        <Row>
          {cards.map((card, index) => (
            <Col lg={4} md={12} key={index}>
              <ServiceCardOne data={card} />
            </Col>
          ))}
        </Row>
      </Container>
      <Lines2
        path="M6 6V134"
        strokeDasharray="128"
        svgSize={{
          width: "12",
          height: "140",
          viewBox: "0 0 12 140"
        }}
        circlePosition={{
          x: "6", y: "6", r: "6"
        }}
        id="line-home-service-1"
      />
      <Lines2
        path="M394 6V90H6V174"
        strokeDasharray="556"
        svgSize={{
          width: "400",
          height: "180",
          viewBox: "0 0 400 180"
        }}
        circlePosition={{
          x: "394", y: "6"
        }}
        id="line-home-service-2"
      />
    </section>
  );
};

export default ServiceHome;
