import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import SectionTitle from "@/components/section-title";

const OurMission = ({ data }) => {

  const { mission, vision } = data;
  return (
    <section className="commonSection our_mission">
      <Container>
        <Row>
          <Col lg={6} sm={12}>
            <div className="ab_detail_wrap">
              <SectionTitle data={mission} />
            </div>
          </Col>
          <Col lg={6} sm={12}>
            <div className="ab_detail_wrap">
              <SectionTitle data={vision} />
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default OurMission;
