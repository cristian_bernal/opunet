import React from "react";
import Image from "next/image";
import { Col, Container, Row } from "react-bootstrap";
import Lines2 from "../Lines/Lines-2";

const AboutSection = ({ data, withlineAnim }) => {
  const { sectionTitle, button, gallery } = data;

  return (
    <section className="commonSection ab_agency">
      <Container>
        <Row>
          <Col lg={6} md={12} sm={12} className="PR_79">
            <h4 className="sub_title">{sectionTitle.subTitle}</h4>
            <h2 className="sec_title dot MB_45">{sectionTitle.title}</h2>
            <p className="sec_desc">{sectionTitle.text}</p>
            {sectionTitle.text2 && (
              <p style={{ marginTop: "-3rem" }} className="sec_desc">{sectionTitle.text2}</p>
            )}
            <a className="common_btn red_bg" href={button.url}>
              <span>{button.name}</span>
            </a>
          </Col>
          <Col lg={6} md={12} sm={12}>
            {gallery.map((item, index) => (
              <div className={`ab_img${index + 1}`} key={index}>
                <Image
                  loading="eager"
                  priority={true}
                  src={item.url}
                  layout='fill'
                />
              </div>
            ))}
          </Col>
        </Row>
      </Container>
      {withlineAnim && (
        <Lines2
          path="M6 6V90H394V174"
          strokeDasharray="556"
          svgSize={{
            width: "400",
            height: "180",
            viewBox: "0 0 400 180"
          }}
          circlePosition={{
            x: "6", y: "6"
          }}
          id="line-home-about"
        />
      )}
    </section>
  );
};

export default AboutSection;
