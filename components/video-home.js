import React, { Fragment, useState } from "react";
import ReactPlayer from 'react-player'
import SectionTitle from "@/components/section-title";
import { Col, Container, Row } from "react-bootstrap";
import Lines2 from "./Lines/Lines-2";
// import ModalVideo from "react-modal-video";
// import "react-modal-video/css/modal-video.min.css";

const VideoHome = ({ data }) => {
  // const [isOpen, setOpen] = useState(false);
  const { sectionTitle, video, preview } = data;
  return (
    <Fragment>
      <section className="commonSection what_wedo border-bottom-0">
        <Container>
          <Row>
            <Col lg={12} className="text-center">
              <SectionTitle data={sectionTitle} />
            </Col>
          </Row>
          <Row>
            <Col lg={12}>
              <div className="videoWrap mb-0">
                <ReactPlayer url={video.url} controls width="100%" height="100%" light={preview.url} />
                {/*  <img src={video.image} alt="" />
                <div className="play_video">
                  <a
                    className="video_popup"
                    onClick={(e) => {
                      e.preventDefault();
                      setOpen(true);
                    }}
                    href="#"
                  >
                    <i className="fa fa-play"></i>
                  </a>
                  <h2>{video.title}</h2>
                </div> */}
              </div>
            </Col>
          </Row>
        </Container>
        <Lines2
          path="M6 6V174"
          strokeDasharray="168"
          svgSize={{
            width: "12",
            height: "180",
            viewBox: "0 0 12 180"
          }}
          circlePosition={{
            x: "6", y: "6", r: "6"
          }}
          id="line-what_wedo"
        />
      </section>
      {/*  <ModalVideo
        channel="youtube"
        autoplay
        isOpen={isOpen}
        videoId={video.ID}
        onClose={() => setOpen(false)}
      /> */}
    </Fragment>
  );
};

export default VideoHome;
