## Opunet front-end

Opunet is a Web Development Digital Agency developed with next js in the front-end and strapi as a headless CMS for the backend

[Live demo](https://opunet.vercel.app/)\
[Strapi API](https://github.com/cristianxew/opunet-backend)

## Table of contents
* [Technologies](#technologies)
* [Setup](#setup)
* [Deployment](#deployment)
* [Screenshots](#screenshots)

## Technologies

##### Libraries
* react - version 17.0.1
* next - 10.0.0
* react-bootstrap - 1.4.3
* bootstrap - 4.6.0
* react-countup - 4.3.3
* react-js-pagination - 3.0.3
* react-scroll - 1.8.1
* react-player - 2.9.0
* react-visibility-sensor - 5.1.1
* sass - 1.35.2
* gsap - 3.5.1
* moment - 2.29.1
* lodash - 4.17.21
* swiper - 6.4.6
* validator - 13.6.0


## Setup

#### **Note**

To edit this website, you'll need to run both the frontend and the [ strapi backend](https://github.com/cristianxew/opunet-backend) in your development environment.

In the project directory, you can run:

* `yarn install`

Install all dependencies 

* `yarn dev`

Runs the app in the development mode. <br/>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

* `yarn build`

Builds the app for production.

## Deployment

You will need to deploy the `frontend` and `backend` projects separately. Here are the docs to deploy each one:

- [Deploy Strapi](https://strapi.io/documentation/v3.x/admin-panel/deploy.html#deployment)
- [Deploy Next.js](https://nextjs.org/docs/deployment)

Don't forget to set up your environment variables on your production apps.

Here are the required ones for the frontend:

- `NEXT_PUBLIC_API_URL`: URL of your Strapi backend, without trailing slash

And for the backend:

- `CLOUDINARY_NAME`, `CLOUDINARY_KEY`, `CLOUDINARY_SECRET`: for uploading media assets to Cloudinary

- `SENDGRID_API_KEY`: for sending Emails


## Screenshots

![image](./assets/readme-imgs/1.png)&nbsp;
![image](./assets/readme-imgs/2.png)&nbsp;
![image](./assets/readme-imgs/3.png)&nbsp;
![image](./assets/readme-imgs/4.png)&nbsp;
![image](./assets/readme-imgs/5.png)&nbsp;